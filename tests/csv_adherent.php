<?php
$pdo = new PDO("mysql:host=127.0.0.1;dbname=iutsd", 'iutsd', 'motdepasse');
$pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false);

unlink("adherent.csv");
$file = fopen("adherent.csv","a");
fwrite($file, "id,intitule,adresse,cp,ville,depot".PHP_EOL);

$stmh = $pdo->query("SELECT id, intitule, adresse, cp, ville, depot, ST_X(localisation) as x,  ST_Y(localisation) as y FROM adherent");
$adherents = $stmh->fetchAll();
foreach ($adherents as $adherent) {
  $x = $adherent['x'];
  $y = $adherent['y'];
  $point = $x ? "POINT($x $y)" : "";
  fwrite($file, "$adherent[id],$adherent[intitule],$adherent[adresse],$adherent[cp],$adherent[ville],$adherent[depot]".PHP_EOL);
}

fclose($file);
