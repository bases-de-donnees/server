<?php
$pdo = new PDO("mysql:host=127.0.0.1;dbname=iutsd", 'iutsd', 'motdepasse');
$pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false);

$sth = $pdo->prepare("SELECT id, depot, ST_X(localisation) as x,  ST_Y(localisation) as y FROM depot");
$sth->execute();
$depots = $sth->fetchAll();

$sth = $pdo->prepare("SELECT * FROM adherent WHERE adresse = '' LIMIT 200");
$sth->execute();
$adherents = $sth->fetchAll();

foreach ($adherents as $adherent){
  $d = rand(0, count($depots) - 1);
  $x = $depots[$d]['x'] + (rand(1, 600) - 300) / 10000.0;
  $y = $depots[$d]['y'] + (rand(1, 600) - 300) / 10000.0;
  $text = file_get_contents("https://api-adresse.data.gouv.fr/reverse/?lon=$x&lat=$y");
  $json = json_decode($text, true);
  $features = $json['features'];
  if (count($features) > 0)
  {
    $f = rand(0, count($features) - 1);
    $adresse = str_replace(",", " ", $features[$f]['properties']['name']);
    $cp = $features[$f]['properties']['postcode'];
    $ville = $features[$f]['properties']['city'];
    $xp = $features[$f]['geometry']['coordinates'][0];
    $yp = $features[$f]['geometry']['coordinates'][1];
    //print_r($features[$f]['properties']['name']);
    print_r("$adherent[id],$adherent[intitule],$d,$adresse,$cp,$ville,POINT($xp $yp)".PHP_EOL);

    $sth = $pdo->prepare("update adherent set depot=?, adresse=?, cp=?, ville=?, localisation=ST_PointFromText(?) WHERE id=?");
    $sth->execute([$d + 1, $adresse, $cp, $ville, "POINT($xp $yp)", $adherent['id']]);
    sleep(3);
  }
  else {
    print_r("$x,$y");
  }
}

/*
update adherent set adresse = '', cp = '', ville='', localisation = null WHERE id in (select id from adherent join (select count(*), adresse, cp, ville from adherent group by 2,3,4 having count(*) > 1) h on (adherent.adresse = h.adresse and adherent.cp = h.cp and adherent.ville = h.ville));
*/
